package com.combasuare;

import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.combasuare.data.Appointment;
import com.combasuare.data.Client;
import com.combasuare.data.ParseableCSV;
import com.combasuare.data.Purchase;
import com.combasuare.data.Service;
import com.combasuare.util.FileReaderUtil;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws ParseException
    {
    	List<Purchase> purchases = getListOfParseableObjects("purchases.csv", new Purchase());
    	List<Appointment> appointments = getListOfParseableObjects("appointments.csv", new Appointment());
    	List<Client> clients = getListOfParseableObjects("clients.csv", new Client());
    	List<Service> services = getListOfParseableObjects("services.csv", new Service());
    	
    	DateFormat df = new SimpleDateFormat("yyyy-MM-DD");
    	Date dateSince = df.parse("2018-01-01");
    	LoyaltyCalculator lc = new LoyaltyCalculator(appointments, services, purchases);
    	Map<Client, Integer> rankingMap = lc.createClientLoyaltyRankingMapSinceDate(clients, dateSince);
    	Map<Client, Integer> sortedMap = rankingMap.entrySet()
    			.stream()
    			.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
    			.collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                LinkedHashMap::new));
    	
    	for (Client client: sortedMap.keySet()) {
    		System.out.println("Loyalty points: " + sortedMap.get(client) + ", Client details: "+ client);
    	}
    }
    
    @SuppressWarnings("unchecked")
	private static <T> List<T> getListOfParseableObjects (String fileName, ParseableCSV object){
    	FileReaderUtil fileReader = new FileReaderUtil();
    	File file = fileReader.getFile(fileName);
    	List<T> list = new ArrayList<>();
    	int counter = 0;
    	for (String line :fileReader.getLinesFromFile(file)) {
    		counter++;
    		try {
				list.add((T) object.parseFromCSV(line));
    		}catch (Exception e) {
    			System.err.println("Failed to parse line: "+ counter +":" + line);
    		}
    	}
    	return list;
    }
    
}
