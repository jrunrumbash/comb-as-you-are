package com.combasuare;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.combasuare.data.Appointment;
import com.combasuare.data.Client;
import com.combasuare.data.Purchase;
import com.combasuare.data.Service;

public class LoyaltyCalculator {
	
	private List<Appointment> appointments;
	private List<Service> services;
	private List<Purchase> purchases;
	
	public LoyaltyCalculator(List<Appointment> appointments, List<Service> services, List<Purchase> purchases) {
		this.appointments = appointments;
		this.services = services;
		this.purchases = purchases;
	}
	
	public int countLoyaltyPoints(Client client){
		return countLoyaltyPointsSinceDate(client, null);
	}
	
	public int countLoyaltyPointsSinceDate(Client client, Date dateSince){
		long dateSinceLong = 0L;
		if (dateSince != null) {
			dateSinceLong = dateSince.getTime();
		}
		String clientId = client.getId();
		List<String> appointmentIds = new ArrayList<>();
		for (Appointment app: appointments) {
			if (app.getClientId().equals(clientId) && app.getEndTime().getTime() >= dateSinceLong ) {
				appointmentIds.add(app.getId());
			}
		}
		int loyaltyPoints = 0;
		for (Service service: services) {
			if (appointmentIds.contains(service.getAppointmentId())) {
				loyaltyPoints += service.getLoyaltyPoints();
			}
		}
		
		for (Purchase purchase: purchases) {
			if(appointmentIds.contains(purchase.getAppointmentId())) {
				loyaltyPoints += purchase.getProductPurchased().getLoyaltyPoints();
			}
		}
		return loyaltyPoints;
	}
	
	public Map<Client, Integer> createClientLoyaltyRankingMap(List<Client> clients){
		return createClientLoyaltyRankingMapSinceDate(clients, null);
	}
	
	public Map<Client, Integer> createClientLoyaltyRankingMapSinceDate(List<Client> clients, Date date){
		Map<Client,Integer> loyaltyMap = new HashMap<>();
		for (Client client: clients) {
			if(client.isBanned()) {
				continue;
			}
			int points = countLoyaltyPoints(client);
			loyaltyMap.put(client, points);
			if(loyaltyMap.size() > 49) {
				break;
			}
		}
		return loyaltyMap;
	}
	
}
