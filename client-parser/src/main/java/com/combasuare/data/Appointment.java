package com.combasuare.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

public class Appointment implements ParseableCSV{
	
	String id;
	String clientId;
	Date startTime;
	Date endTime;
	
	public Appointment() {
		
	}

	public Appointment(String id, String clientId, Date startTime, Date endTime) {
		super();
		this.id = id;
		this.clientId = clientId;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Override
	public Appointment parseFromCSV(String line){
		StringTokenizer splitter = new StringTokenizer(line, ",");
		String id = splitter.nextToken();
		String clientId = splitter.nextToken();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-DD hh:mm:ss z");
		Date startTime;
		Date endTime;
		try {
			startTime = dateFormat.parse(splitter.nextToken());
			endTime = dateFormat.parse(splitter.nextToken());
		}catch (ParseException e) {
			throw new IllegalArgumentException("Could not format CSV line, format of the dates is incorrect");
		}
		return new Appointment(id, clientId, startTime, endTime);
	}

	@Override
	public int hashCode() {
		final int prime = 97;
		int result = 1;
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", clientId=" + clientId + ", startTime=" + startTime + ", endTime=" + endTime
				+ "]";
	}
	
}
