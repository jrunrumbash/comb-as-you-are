package com.combasuare.data;

import java.util.StringTokenizer;

public class Client implements ParseableCSV {

	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private long phone;
	private Gender gender;
	private boolean banned;
	
	public Client() {};

	public Client(String id, String firstName, String lastName, String email, long phone, Gender gender,
			boolean banned) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.banned = banned;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long l) {
		this.phone = l;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public boolean isBanned() {
		return banned;
	}
	public void setBanned(boolean banned) {
		this.banned = banned;
	}

	@Override
	public Client parseFromCSV(String line) {
		StringTokenizer splitter = new StringTokenizer(line, ",");
		String id = splitter.nextToken();
		String firstName = splitter.nextToken();
		String lastName = splitter.nextToken();
		String email = splitter.nextToken();
		long phone = parsePhoneNumber(splitter.nextToken());
		Gender gender = parseGender(splitter.nextToken());
		boolean banned = splitter.nextToken().equalsIgnoreCase("true");
		return new Client(id, firstName, lastName, email, phone, gender, banned);
	}

	private Gender parseGender(String nextToken) {
		if(nextToken.equalsIgnoreCase("male")) {
			return Gender.Male;
		}else if (nextToken.equalsIgnoreCase("female")) {
			return Gender.Female;
		}else {
			throw new IllegalArgumentException("Could not parse the gender from string " + nextToken);
		}
	}

	private long parsePhoneNumber(String nextToken) {
		String cleanedNumber = nextToken.replaceAll("[^\\d]", "");
		Long phoneNumber = new Long(cleanedNumber);
		return phoneNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + (int) (phone ^ (phone >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "Client [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", phone=" + phone
				+ ", gender=" + gender + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone != other.phone)
			return false;
		return true;
	}
}
