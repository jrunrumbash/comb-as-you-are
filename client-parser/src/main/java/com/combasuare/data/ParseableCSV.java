package com.combasuare.data;

public interface ParseableCSV {
	
	 ParseableCSV parseFromCSV(String line);

}
