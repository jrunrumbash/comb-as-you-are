package com.combasuare.data;

import java.util.StringTokenizer;

public class Purchase implements ParseableCSV{
	
	private String id;
	private String appointmentId;
	private Product productPurchased;
	
	public Purchase() {
		
	}

	public Purchase(String id, String appointmentId, Product productPurchased) {
		super();
		this.id = id;
		this.appointmentId = appointmentId;
		this.productPurchased = productPurchased;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Product getProductPurchased() {
		return productPurchased;
	}

	public void setProductPurchased(Product productPurchased) {
		this.productPurchased = productPurchased;
	}

	@Override
	public Purchase parseFromCSV(String line) {
		StringTokenizer splitter = new StringTokenizer(line, ",");
		String id = splitter.nextToken();
		String appointmentId = splitter.nextToken();
		String name = splitter.nextToken();
		double price = new Double(splitter.nextToken());
		int loyalytPoints = new Integer(splitter.nextToken());
		Product product = new Product(price, loyalytPoints, name);
		return new Purchase(id, appointmentId, product);
	}

	@Override
	public int hashCode() {
		final int prime = 57;
		int result = 1;
		result = prime * result + ((appointmentId == null) ? 0 : appointmentId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((productPurchased == null) ? 0 : productPurchased.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Purchase other = (Purchase) obj;
		if (appointmentId == null) {
			if (other.appointmentId != null)
				return false;
		} else if (!appointmentId.equals(other.appointmentId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (productPurchased == null) {
			if (other.productPurchased != null)
				return false;
		} else if (!productPurchased.equals(other.productPurchased))
			return false;
		return true;
	}
	
	
	
}
