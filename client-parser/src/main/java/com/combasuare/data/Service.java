package com.combasuare.data;

import java.util.StringTokenizer;

public class Service implements ParseableCSV {
	
	private String id;
	private String appointmentId;
	private String name;
	private double price;
	private int loyaltyPoints;
	
	public Service() {};

	public Service(String id, String appointmentId, String name, double price, int loyaltyPoints) {
		super();
		this.id = id;
		this.appointmentId = appointmentId;
		this.name = name;
		this.price = price;
		this.loyaltyPoints = loyaltyPoints;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appointmentId == null) ? 0 : appointmentId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + loyaltyPoints;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (appointmentId == null) {
			if (other.appointmentId != null)
				return false;
		} else if (!appointmentId.equals(other.appointmentId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (loyaltyPoints != other.loyaltyPoints)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		return true;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(int loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	@Override
	public Service parseFromCSV(String line) {
		StringTokenizer splitter = new StringTokenizer(line, ",");
		String id = splitter.nextToken();
		String appointmentId = splitter.nextToken();
		String name = splitter.nextToken();
		Double price = new Double(splitter.nextToken());
		Integer loyaltyPoints = new Integer(splitter.nextToken());
		return new Service(id, appointmentId, name, price, loyaltyPoints);
	}

}
