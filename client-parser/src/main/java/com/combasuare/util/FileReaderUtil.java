package com.combasuare.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReaderUtil {
	
	public List<String> getLinesFromFile(File file){
		List<String> linesFromFile = new ArrayList<>();
		try (Scanner scanner = new Scanner(file)){
			while(scanner.hasNext()) {
				linesFromFile.add(scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return linesFromFile;
	}
	
	public File getFile(String fileName) {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(fileName).getFile());
	}

}
