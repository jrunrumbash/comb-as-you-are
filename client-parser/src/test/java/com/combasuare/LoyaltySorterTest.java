package com.combasuare;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.combasuare.data.Appointment;
import com.combasuare.data.Client;
import com.combasuare.data.Gender;
import com.combasuare.data.Product;
import com.combasuare.data.Purchase;
import com.combasuare.data.Service;

public class LoyaltySorterTest {

	@Test
	public void testCountingTheLoyaltyPoints() throws ParseException {
		List<Service> serviceProvided = createTestServices();
		List<Purchase> purchaseCompleted = createTestPurchases();
		List<Appointment> appointments =createTestAppointments();
		LoyaltyCalculator lc = new LoyaltyCalculator(appointments, serviceProvided, purchaseCompleted);
		Client client = new Client("cid3", "Sean", "Mc Caulin", 
				 "sean.mc.caulin@gmail.com", 589632475l, Gender.Male, false);
		int points = lc.countLoyaltyPoints(client);
		assertEquals(100,points);

	}
	
	@Test 
	public void testFilteringLoyaltyPointSinceDate() throws ParseException {
		List<Service> serviceProvided = createTestServices();
		List<Purchase> purchaseCompleted = createTestPurchases();
		List<Appointment> appointments =createTestAppointments();
		LoyaltyCalculator lc = new LoyaltyCalculator(appointments, serviceProvided, purchaseCompleted);
		Client client = new Client("cid3", "Sean", "Mc Caulin", 
				 "sean.mc.caulin@gmail.com", 589632475l, Gender.Male, false);
    	DateFormat df = new SimpleDateFormat("yyyy-MM-DD");
    	Date date = df.parse("2018-01-01");
		int points = lc.countLoyaltyPointsSinceDate(client, date);
		assertEquals(80,points);
		
	}
	
	@Test
	public void testExclusionOfBannedClients() throws ParseException {
		List<Service> serviceProvided = createTestServices();
		List<Purchase> purchaseCompleted = createTestPurchases();
		List<Appointment> appointments = createTestAppointments();
		List<Client> clients = createTestClients();
		LoyaltyCalculator lc = new LoyaltyCalculator(appointments, serviceProvided, purchaseCompleted);
		Map<Client, Integer> loyaltyMap = lc.createClientLoyaltyRankingMap(clients);
		assertEquals(2, loyaltyMap.size());
		Client client = clients.get(1);
		client.setBanned(false);
		loyaltyMap = lc.createClientLoyaltyRankingMap(clients);
		assertEquals(3, loyaltyMap.size());
	}
	
	private List<Appointment> createTestAppointments() throws ParseException{
    	DateFormat df = new SimpleDateFormat("yyyy-MM-DD");
    	Date date = df.parse("2018-01-01");
    	Date date2 = df.parse("2017-01-01");
    	Date date3 = df.parse("2019-01-01");
		List<Appointment> appointments = Arrays.asList(
		 new Appointment("appid1", "cid1", date, date),
		 new Appointment("appid2", "cid2", date, date),
		 new Appointment("appid3", "cid3", date, date),
		 new Appointment("appid4", "cid1", date, date),
		 new Appointment("appid5", "cid2", date, date),
		 new Appointment("appid6", "cid3", date2, date2),
		 new Appointment("appid7", "cid1", date, date),
		 new Appointment("appid8", "cid2", date, date),
		 new Appointment("appid9", "cid3", date3, date3));
		return appointments;
	}

	private List<Purchase> createTestPurchases(){
		List<Purchase> purchases = Arrays.asList(
		 new Purchase("pid1", "appid5", new Product(1.20, 10, "Nail Polish")),
		 new Purchase("pid2", "appid6", new Product(2.20, 20, "Shampoo")),
		 new Purchase("pid3", "appid7", new Product(2.10, 20, "Conditioner")),
		 new Purchase("pid4", "appid8", new Product(4.20, 40, "Hair colour")),
		 new Purchase("pid5", "appid9", new Product(2.20, 20, "Shampoo")));
		return purchases;
	}

	private List<Client> createTestClients(){
		List<Client> clients = Arrays.asList(
		 new Client("cid1", "Jane", "Doe", "jane.doe@gmail.com", 865547953l, Gender.Female, false),
		 new Client("cid2", "John", "Garry", "john.garry@gmail.com", 586423589l, Gender.Male, true),
		 new Client("cid3", "Sean", "Mc Caulin", "sean.mc.caulin@gmail.com", 589632475l, Gender.Male, false));
		return clients;
	}

	private List<Service> createTestServices(){
		List<Service> services = Arrays.asList(
		 new Service("sid1", "appid1", "Hair Wash", 4.50, 10),
		 new Service("sid2", "appid2", "Manicure", 5.50, 70),
		 new Service("sid3", "appid3", "Beard Trim", 7.50, 60),
		 new Service("sid4", "appid4", "Hair Wash", 4.50, 30));
		return services;
	}
}
