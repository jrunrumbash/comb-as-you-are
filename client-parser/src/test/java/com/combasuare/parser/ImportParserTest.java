package com.combasuare.parser;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.junit.Test;

import com.combasuare.data.Appointment;
import com.combasuare.data.Client;
import com.combasuare.data.Gender;
import com.combasuare.data.Product;
import com.combasuare.data.Purchase;
import com.combasuare.data.Service;

public class ImportParserTest {

	@Test
	public void testParseForPurchases() {
		String testString = "d2d3b92d-f9b5-48c5-bf31-88c28e3b73ac,7416ebc3-12ce-4000-87fb-82973722ebf4,Shampoo,19.5,20";
		Purchase newPurchasee = new Purchase().parseFromCSV(testString);
		Purchase expectedPurchase = new Purchase();
		expectedPurchase.setId("d2d3b92d-f9b5-48c5-bf31-88c28e3b73ac");
		expectedPurchase.setAppointmentId("7416ebc3-12ce-4000-87fb-82973722ebf4");
		Product product = new Product(19.5, 20, "Shampoo");
		expectedPurchase.setProductPurchased(product);

		assertEquals(expectedPurchase, newPurchasee);
	}
	
	@Test
	public void testParseForAppointments() throws Exception {
		String testCSVString = "7416ebc3-12ce-4000-87fb-82973722ebf4,263f67fa-ce8f-447b-98cf-317656542216,2016-02-07 17:15:00 +0000,2016-02-07 20:15:00 +0000";
		Appointment newAppointment = new Appointment().parseFromCSV(testCSVString);
		Appointment expectedAppointment = new Appointment();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-DD hh:mm:ss z");
		expectedAppointment.setId("7416ebc3-12ce-4000-87fb-82973722ebf4");
		expectedAppointment.setClientId("263f67fa-ce8f-447b-98cf-317656542216");
		expectedAppointment.setStartTime(dateFormat.parse("2016-02-07 17:15:00 +0000"));
		expectedAppointment.setEndTime(dateFormat.parse("2016-02-07 20:15:00 +0000"));
		
		assertEquals(expectedAppointment, newAppointment);
	}
	
	@Test
	public void testParseForClient() throws Exception {
		String testCSVString = "e0b8ebfc-6e57-4661-9546-328c644a3764,Dori,Dietrich,patrica@keeling.net,(272) 301-6356,Male,false";
		Client newClient = new Client().parseFromCSV(testCSVString);
		Client expectedClient = new Client();
		expectedClient.setId("e0b8ebfc-6e57-4661-9546-328c644a3764");
		expectedClient.setFirstName("Dori");
		expectedClient.setLastName("Dietrich");
		expectedClient.setEmail("patrica@keeling.net");
		expectedClient.setPhone(2723016356L);
		expectedClient.setGender(Gender.Male);
		expectedClient.setBanned(false);
		
		assertEquals(expectedClient, newClient);
	}
	
	@Test
	public void testParseForService() throws Exception {
		String testCSVString = "f1fc7009-0c44-4f89-ac98-5de9ce58095c,7416ebc3-12ce-4000-87fb-82973722ebf4,Full Head Colour,85.0,80";
		Service newService = new Service().parseFromCSV(testCSVString);
		Service expectedService = new Service();
		expectedService.setId("f1fc7009-0c44-4f89-ac98-5de9ce58095c");
		expectedService.setAppointmentId("7416ebc3-12ce-4000-87fb-82973722ebf4");
		expectedService.setName("Full Head Colour");
		expectedService.setPrice(85.0);
		expectedService.setLoyaltyPoints(80);
		
		assertEquals(expectedService, newService);
	}

}
