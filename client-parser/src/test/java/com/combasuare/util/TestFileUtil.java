package com.combasuare.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import org.junit.Test;

public class TestFileUtil {
	private static String TEST_STRING = "This is a Test";
	
	@Test
	public void testReadingLinesFromFile() {
		
		String filename = "testFiles/ReadingLineByLine";
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(filename).getFile());
		List<String> linesFromFile = new FileReaderUtil().getLinesFromFile(file);
		assertEquals(3,linesFromFile.size());
		for (String line: linesFromFile) {
			assertEquals(TEST_STRING, line);
		}
	}

}
